//
//  Key+Helper.swift
//  toolbox-test
//
//  Created by Daniel Orellana on 23/06/22.
//

import Foundation

var EXIST_SESSION_KEY = "exist_session_tb"
var AUTHORIZATION_KEY = "authorization_tb"

var LOGIN_VIEW = "LoginView"
var HOME_VIEW = "HomeView"
var HOME_NAVIGATION_VIEW = "HomeNavigationView"
