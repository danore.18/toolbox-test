//
//  Extension+Helper.swift
//  toolbox-test
//
//  Created by Daniel Orellana on 23/06/22.
//

import Foundation

extension URLComponents {
    
    mutating func setQueryItems(with parameters: [String: Any]) {
        self.queryItems = parameters.map { URLQueryItem(name: $0.key, value: $0.value as? String) }
    }
    
}

extension Notification.Name {
    static let didSessionExpired = Notification.Name("didSessionExpired")
}
