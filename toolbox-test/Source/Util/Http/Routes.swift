//
//  Routes.swift
//  toolbox-test
//
//  Created by Daniel Orellana on 23/06/22.
//

import Foundation

let API_BASE = "https://echo-serv.tbxnet.com"

let AUTH_URL = "\(API_BASE)/v1/mobile/auth"
let MOVIES_URL = "\(API_BASE)/v1/mobile/data"
