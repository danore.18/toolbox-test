//
//  HttpMethod.swift
//  toolbox-test
//
//  Created by Daniel Orellana on 23/06/22.
//

import Foundation

enum HttpMethod: String {
    
    case post
    case put
    case get
    case delete
    
    var value: String? {
        switch self {
        case .post:
            return "POST"
        case .put:
            return "PUT"
        case .get:
            return "GET"
        case .delete:
            return "DELETE"
        }
    }
}
