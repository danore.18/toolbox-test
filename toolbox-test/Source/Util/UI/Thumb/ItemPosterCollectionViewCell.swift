//
//  ItemPosterCollectionViewCell.swift
//  toolbox-test
//
//  Created by Daniel Orellana on 23/06/22.
//

import Foundation
import UIKit

class ItemPosterCollectionViewCell : UICollectionViewCell {
    @IBOutlet weak var imgCover: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
}
