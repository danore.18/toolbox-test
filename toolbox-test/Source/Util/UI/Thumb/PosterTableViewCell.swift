//
//  PosterTableViewCell.swift
//  toolbox-test
//
//  Created by Daniel Orellana on 23/06/22.
//

import Foundation
import UIKit

class PosterTableViewCell: UITableViewCell {
    @IBOutlet weak var collectionView: UICollectionView!
}
