//
//  AuthModel.swift
//  toolbox-test
//
//  Created by Daniel Orellana on 23/06/22.
//

import Foundation

struct AuthModel: Codable {
    
    var sub: String
    var token: String
    var type: String
    
}
