//
//  ErrorModel.swift
//  toolbox-test
//
//  Created by Daniel Orellana on 23/06/22.
//

import Foundation

struct ErrorModel: Codable {
    var code: String
    var message: String
}
