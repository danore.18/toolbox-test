//
//  MovieModel.swift
//  toolbox-test
//
//  Created by Daniel Orellana on 23/06/22.
//

import Foundation

struct MovieModel: Codable {
    var title: String
    var type: String
    var items: [ItemModel]
}

struct ItemModel: Codable {
    var title: String
    var imageUrl: String?
    var videoUrl: String?
    var description: String
}
